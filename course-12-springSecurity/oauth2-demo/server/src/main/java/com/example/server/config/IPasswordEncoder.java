package com.example.server.config;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 创建PasswordEncorder的实现类
 */
public class IPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return s.equals(charSequence.toString());
    }
}
