package com.example.server;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@RestController
public class ResourceController {

    @GetMapping("/re")
    public Object re() {
        return "这是资源服务器的资源rrr";
    }

    @GetMapping("/ll")
    public Object ll() {
        return "这是资源服务器的资源lll";
    }

}
