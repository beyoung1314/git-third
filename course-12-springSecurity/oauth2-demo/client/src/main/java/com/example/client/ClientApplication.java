package com.example.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@SpringBootApplication
public class ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/client")
    public Object client(Authentication authentication) {
        return authentication;
    }

    @GetMapping("/index")
    public Object index() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth("client", "secret");
        MultiValueMap<String, String> params= new LinkedMultiValueMap<>();
        params.add("grant_type","password");
        params.add("username","user");
        params.add("password","1");
        params.add("redirect_uri","http://www.baidu.com");

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://localhost:8081/oauth/token", requestEntity, String.class);

        String token = responseEntity.getBody();

        return "这是client的index -----> " + token;
    }

}
