package com.example.common.event.handler;

import com.example.common.event.MessageEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MessageEventHandler implements ApplicationListener<MessageEvent> {

    /**
     * @Async 表示异步调用
     */
    @Async
    @Override
    public void onApplicationEvent(MessageEvent event) {

        //TODO 业务处理
        log.info("---------------> " + event.toString());
    }
}
