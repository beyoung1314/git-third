package com.example.common.event;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class MessageEvent extends ApplicationEvent {

    private long fromUserId;
    private long toUserId;
    private long postId;

    private int event;

    public MessageEvent(Object source, int event) {
        super(source);
        this.event = event;
    }
}
