package com.example.config;

import com.example.annotation.ConditionalOnSystem;
import org.springframework.context.annotation.Bean;

/**
 * @author 吕一明
 * @公众号 码客在线
 * @since ${date} ${time}
 */
@ConditionalOnSystem(system = "linux")
public class HelloWorldConfig2 {

    @Bean
    Lv lv () {
        System.out.println("-------------->lv 初始化222222");
        return new Lv();
    }

}
