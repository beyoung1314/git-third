package com.example.annotation;

import com.example.config.HelloWorldConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author 吕一明
 * @公众号 码客在线
 * @since ${date} ${time}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({HelloWorldConfig.class})
public @interface HelloWorld {
}
