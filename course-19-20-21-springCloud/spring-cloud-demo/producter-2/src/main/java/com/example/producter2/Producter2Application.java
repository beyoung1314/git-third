package com.example.producter2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Producter2Application {

    public static void main(String[] args) {
        SpringApplication.run(Producter2Application.class, args);
    }

}
