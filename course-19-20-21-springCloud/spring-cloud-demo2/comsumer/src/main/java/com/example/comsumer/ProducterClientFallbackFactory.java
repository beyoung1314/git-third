package com.example.comsumer;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Component
public class ProducterClientFallbackFactory implements FallbackFactory<ProducterClient> {
    @Override
    public ProducterClient create(Throwable throwable) {
        System.out.println(throwable.toString());

        return new ProducterClient() {
            @Override
            public String findById(Long id) {
                return "fallback feign client2 - " + id;
            }
        };
    }
}
