package com.example;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author 吕一明
 * @公众号 码客在线
 */
@Component
public class SimpleJob {

    @Scheduled(cron = "0/2 * * * * ?")
    public void test() {
        System.out.println("22222222222222");
    }

}
